#include <iostream>
#include "pessoa.cpp"
#include "amigo.cpp"
#include "contato.cpp"

using namespace std;


int main () {
	
	Pessoa x;
	Amigo y;
	Contato z;
	
	x.setNome ("Renata");
	x.setIdade ("18");
	x.setTelefone ("555-5555");
	
	y.setApelido ("Rê");
	y.setEmail ("...@...");
	
	z.setCelular ("0000-0000");
	z.setEndereco ("Brasilia");
 		
	cout << "Nome: " << x.getNome() << endl;
	cout << "Idade: " << x.getIdade() << endl;
	cout << "Telefone: " << x.getTelefone() << endl;
	cout << "Apelido: " << y.getApelido() << endl;
        cout << "Email: " << y.getEmail() << endl;
	cout << "Celular: " << z.getCelular() << endl;
        cout << "Endereco: " << z.getEndereco() << endl;

	
	return 0;
}